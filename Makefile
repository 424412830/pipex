NAME				= pipex

################## FILES & DIRECTORIES ###################################################

SRCS_FILES			= pipex.c	pipex_utils.c	clean.c
SRCS_DIR			= srcs/
SRCS				= $(addprefix $(SRCS_DIR),$(SRCS_FILES))

OBJS_FILES			= $(SRCS:.c=.o)
OBJS_DIR			= objs/
#OBJS				= $(addprefix $(OBJS_DIR),$(OBJS_FILES))

HEADER_DIR			= include/
HEADER_FILE			= pipex.h
HEADER				= $(addprefix ${HEADER_DIR}, ${HEADER_FILE})

LIBFT				= libft/libft.a
LIBFT_PATH			= libft/

###################### BONUS ############################################################

NAME_BONUS			= pipex_bonus

SRCS_FILES_BONUS	= pipex_bonus.c	pipex_utils_bonus.c	clean_bonus.c	here_doc.c
SRCS_BONUS			= $(addprefix $(SRCS_DIR),$(SRCS_FILES_BONUS))

OBJS_FILES_BONUS	= $(SRCS_BONUS:.c=.o)
OBJS_DIR			= objs/

HEADER_FILE_BONUS	= pipex_bonus.h
HEADER_BONUS		= $(addprefix ${HEADER_DIR}, ${HEADER_FILE_BONUS})


#########################################################################################

CC					= cc

FLAGS				= -Wall -Wextra -Werror -g3

RM					= rm -rf


###################### RULES ##########################################################


%.o: %.c
				${CC} ${FLAGS} -c $< -o ${<:.c=.o}

${NAME}:		${OBJS_FILES} ${HEADER} ${LIBFT}
				${CC} ${OBJS_FILES} ${LIBFT} -o ${NAME}
				mkdir -p ${OBJS_DIR}
				mv ${OBJS_FILES} ${OBJS_DIR}

${LIBFT}:
				make -C ${LIBFT_PATH}

all:			${NAME}

clean :
				make -C ${LIBFT_PATH} clean
				${RM} ${OBJS_DIR}

fclean : 		clean
				make -C ${LIBFT_PATH} fclean
				${RM} ${NAME}
				${RM} ${NAME_BONUS}

re : 			fclean all

bonus : 		${OBJS_FILES_BONUS} ${HEADER_BONUS} ${LIBFT}
				${CC} ${OBJS_FILES_BONUS} ${LIBFT} -o ${NAME_BONUS}
				mkdir -p ${OBJS_DIR}
				mv ${OBJS_FILES_BONUS} ${OBJS_DIR}

.SECONDARY :	$(OBJS_FILES) $(OBJS_FILES_BONUS)

.PHONY :		all clean fclean re bonus so
