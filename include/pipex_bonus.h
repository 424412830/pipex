/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex_bonus.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/04 15:30:28 by abinet            #+#    #+#             */
/*   Updated: 2023/08/06 19:18:51 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_BONUS_H

# define PIPEX_BONUS_H
# include "../libft/libft.h"
# include <fcntl.h>
# include <sys/wait.h>
# include <stdio.h>
# include <unistd.h>

typedef struct s_data
{
	int		fdin;
	int		fdout;
	char	**cmd1;
	char	**cmd2;
	char	**cmd3;
	char	*path_cmd1;
	char	*path_cmd2;
	char	*path_cmd3;
	int		pipefd[2];
	int		pipefd2[2];
	int		argc;
	int		index_arg;
	int		here_doc;
}				t_data;

char	*find_path(char *infile, char **envp);
char	**find_all_paths(char **envp);
void	child(t_data data, char **envp);
void	child2(t_data data, char **envp);
void	child3(t_data data, char **envp);
int		pipex(t_data data, char **argv, char **envp);
void	free_tab(char **res);
void	swap_int(int pipefd[2], int pipefd2[2]);
void	free_n_close(t_data data, int val_pipex);
t_data	set_cmd1(t_data data, char **argv, char **envp);
t_data	set_cmd2(t_data data, char **argv, char **envp);
t_data	set_cmd3(t_data data, char **argv, char**envp);
int		here_doc(char *limiter);

#endif
