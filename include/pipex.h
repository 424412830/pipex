/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/25 09:33:11 by abinet            #+#    #+#             */
/*   Updated: 2023/08/07 15:34:47 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H

# define PIPEX_H
# include "../libft/libft.h"
# include <fcntl.h>
# include <sys/wait.h>
# include <stdio.h>
# include <unistd.h>

typedef struct s_data
{
	int		fdin;
	int		fdout;
	char	**cmd1;
	char	**cmd2;
	char	*path_cmd1;
	char	*path_cmd2;
	int		pipefd[2];
}				t_data;

char	*find_path(char *infile, char **envp);
char	**find_all_paths(char **envp);
void	child(t_data data, char **envp);
void	child2(t_data data, char **envp);
int		pipex(t_data data, char **argv, char **envp);
int		pipex2(t_data data, char **argv, char **envp);
void	free_tab(char **res);
void	free_n_close(t_data data, int val_pipex);
t_data	set_cmd1(t_data data, char **argv, char **envp);
t_data	set_cmd2(t_data data, char **argv, char**envp);
#endif
