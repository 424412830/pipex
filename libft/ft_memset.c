/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/08 14:54:14 by abinet            #+#    #+#             */
/*   Updated: 2022/11/16 17:09:20 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stddef.h>
#include <stdio.h>

void	*ft_memset(void *s, int c, size_t n)
{
	long unsigned int	i;
	unsigned char		*a;

	i = 0;
	a = s;
	if (n <= 0)
		return (s);
	while (i < n)
	{
		a[i] = c;
		i++;
	}
	return (s);
}
/*
int	main(int argc, char **argv)
{
	char	*a;
	int		i;

	(void) argc;
	a = argv[1];
	for (i = 0; i < 5; i++)
		printf("%d\n", a[i]);
	printf("\n");
	ft_memset(a, 116, 5);
	for (i = 0; i < 5; i++)
		printf("%d\n", a[i]);
	return (0);
}*/
