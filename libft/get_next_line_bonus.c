/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/14 10:28:39 by abinet            #+#    #+#             */
/*   Updated: 2023/03/24 11:46:20 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*get_next_line_bonus(int fd)
{
	char		*next_line;
	static char	end_buf[1024][BUFFER_SIZE + 1];
	int			i;
	int			j;

	if (fd < 0 || BUFFER_SIZE <= 0 || fd > 1024)
		return (NULL);
	i = 0;
	while (end_buf[fd][i] != '\0' && end_buf[fd][i] != '\n')
			i++;
	next_line = ft_substr(end_buf[fd], 0, i + 1);
	if (end_buf[fd][i] == '\n')
	{
		j = 0;
		while (end_buf[fd][i++])
			end_buf[fd][j++] = end_buf[fd][i];
		return (next_line);
	}
	next_line = add_line_bonus(fd, next_line, end_buf[fd]);
	if (ft_strlen(next_line) == 0)
		return (free(next_line), NULL);
	return (next_line);
}

char	*add_line_bonus(int fd, char *next_line, char *end_buf)
{
	int		bytes_read;
	char	*buffer;
	char	*temp;
	int		i;
	int		j;

	buffer = malloc(sizeof(char) * (BUFFER_SIZE + 1));
	if (!buffer)
		return (NULL);
	buffer[0] = '\0';
	while (!ft_strchr(buffer, '\n'))
	{
		next_line = ft_strjoin_gnl(next_line, buffer);
		bytes_read = read(fd, buffer, BUFFER_SIZE);
		if (bytes_read <= 0)
			return (end_buf[0] = '\0', free(buffer), next_line);
		buffer[bytes_read] = '\0';
	}
	i = (ft_strlen(buffer) - ft_strlen(ft_strchr(buffer, '\n')));
	j = 0;
	temp = ft_substr(buffer, 0, i + 1);
	next_line = ft_strjoin_gnl(next_line, temp);
	while (i < BUFFER_SIZE && j < bytes_read)
		end_buf[j++] = buffer[++i];
	return (free(temp), free(buffer), next_line);
}
