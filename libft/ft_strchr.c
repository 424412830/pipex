/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/10 22:35:34 by abinet            #+#    #+#             */
/*   Updated: 2022/11/30 16:30:03 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <string.h>

char	*ft_strchr(char *str, int c)
{
	char	cc;

	if (!str)
		return (NULL);
	cc = (char)c;
	while (*str)
	{
		if (*str == cc)
			return (str);
		str++;
	}
	if (cc == '\0')
		return (str);
	return (NULL);
}
/*
int	main(int argc, char **argv)
{
	(void) argc;
	printf("%p\n", ft_strchr(argv[1], argv[2][0]));
	printf("%p\n", strchr(argv[1], argv[2][0]));
	return (0);
}
*/
