/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base_cnt.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/09 15:25:25 by abinet            #+#    #+#             */
/*   Updated: 2022/12/15 11:34:08 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>

int	ft_putnbr_base_cnt(unsigned long n, char *base, int fd, int *count)
{
	long	size;

	size = (long)ft_strlen(base);
	if (n >= (unsigned long)size)
		ft_putnbr_base_cnt(n / size, base, fd, count);
	(*count) += ft_putchar_cnt(base[n % size], fd);
	return (*count);
}

int	ft_putptr_cnt(unsigned long n, int fd)
{
	int	count;

	count = 0;
	if (n == 0)
	{
		ft_putstr_cnt("(nil)", 1);
		return (5);
	}
	else
	{
		count += ft_putstr_cnt("0x", 1);
		ft_putnbr_base_cnt(n, HEXA_LOW, fd, &count);
	}
	return (count);
}

// int	main(int argc, char **argv)
// {
// 	//long	i;

// 	(void) argc;
// 	(void) argv;
// 	// for (i = 0; i < 300; i+= 1)
// 	// {
// 	// 	ft_putnbr_base(i, argv[1], 1);
// 	// 	ft_putchar_fd('\n', 1);
// 	// }
// 	ft_putptr_base(atoi(argv[1]), 1);
// 	return (0);
// }
