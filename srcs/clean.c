/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/05 19:18:44 by abinet            #+#    #+#             */
/*   Updated: 2023/08/07 17:56:39 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pipex.h"

void	free_tab(char **res)
{
	int	i;

	i = 0;
	while (res[i])
	{
		free(res[i]);
		i++;
	}
	free(res);
}

void	free_n_close(t_data data, int val_error)
{
	if (val_error <= 8 && val_error != 5 && val_error != 3)
		perror("Error: ");
	if (val_error >= 1 && val_error < 5)
		close(data.fdin);
	if (val_error >= 2 && val_error < 5)
		close(data.pipefd[1]);
	if (val_error >= 2 && val_error != 3 && data.fdin != -1)
		close(data.pipefd[0]);
	if (val_error == 3)
	{
		ft_putstr_fd("Error: No such file or directory\n", 2);
		free_tab(data.cmd1);
	}
	if (val_error >= 4 && data.fdin != -1 && data.path_cmd1)
		free_tab(data.cmd1);
	if (val_error >= 4 && data.fdin != -1 && data.path_cmd1)
		free(data.path_cmd1);
	if (val_error >= 6)
		close(data.fdout);
	if (val_error >= 7)
		free_tab(data.cmd2);
	if (val_error >= 8)
		free(data.path_cmd2);
	waitpid(-1, NULL, 0);
}
