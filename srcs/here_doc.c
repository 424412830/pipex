/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   here_doc.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/06 14:55:54 by abinet            #+#    #+#             */
/*   Updated: 2023/08/07 15:07:00 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pipex_bonus.h"

int	here_doc(char *limiter)
{
	char	*line;
	size_t	len;
	int		fd_temp;

	fd_temp = open(".here_doc", O_CREAT | O_RDWR | O_TRUNC, 0777);
	while (1)
	{
		ft_printf("heredoc> ");
		line = get_next_line(STDIN_FILENO);
		len = ft_strlen(line);
		line[len - 1] = '\0';
		if (ft_strncmp(line, limiter, len) == 0)
			break ;
		ft_putstr_fd(line, fd_temp);
		free(line);
	}
	close(fd_temp);
	return (fd_temp);
}
