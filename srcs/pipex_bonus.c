/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex_bonus.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/04 15:23:21 by abinet            #+#    #+#             */
/*   Updated: 2023/08/06 20:11:24 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pipex_bonus.h"



void	child3(t_data data, char **envp)
{
	close(data.pipefd[1]);
	dup2(data.pipefd[0], STDIN_FILENO);
	close(data.pipefd[0]);
	dup2(data.fdout, STDOUT_FILENO);
	close(data.fdout);
	execve(data.path_cmd3, data.cmd3, envp);
}

void	child2(t_data data, char **envp)
{
	close(data.fdin);
	close(data.pipefd[1]);
	dup2(data.pipefd[0], STDIN_FILENO);
	close(data.pipefd[0]);
	dup2(data.pipefd2[1], STDOUT_FILENO);
	close(data.pipefd2[1]);
	execve(data.path_cmd2, data.cmd2, envp);
}

void	child(t_data data, char **envp)
{
	close(data.pipefd[0]);
	dup2(data.fdin, STDIN_FILENO);
	close(data.fdin);
	dup2(data.pipefd[1], STDOUT_FILENO);
	close(data.pipefd[1]);
	execve(data.path_cmd1, data.cmd1, envp);
}

// void	child2(t_data data, char **envp)
// {
// 	close(data.pipefd[1]);
// 	dup2(data.pipefd[0], STDIN_FILENO);
// 	close(data.pipefd[0]);
// 	dup2(data.pipefd2[1], STDOUT_FILENO);
// 	close(data.pipefd2[1]);
// 	execve(data.path_cmd2, data.cmd2, envp);
// }


int	pipex(t_data data, char **argv, char **envp)
{
	pid_t	pid;
	pid_t	pid2;
	pid_t	pid3;
	int		i;

	data = set_cmd1(data, argv, envp);
	pid = fork();
	if (pid == -1)
		return (free_n_close(data, 4), 1);
	if (pid == 0)
	{
		child(data, envp);
		return (free_n_close(data, 4), 1);
	}
	close(data.fdin);
	close(data.pipefd[1]);
	i = 0;
	while (i <= (data.argc - data.here_doc) - 6)
	{
		data.index_arg = i + 3 + data.here_doc;
		data = set_cmd2(data, argv, envp);
		pid2 = fork();
		if (pid2 == -1)
			return (free_n_close(data, 8), 1);
		if (pid2 == 0)
			child2(data, envp);
		free_tab(data.cmd2);
		i++;
		close(data.pipefd[0]);
		close(data.pipefd2[1]);
	}
	data = set_cmd3(data, argv, envp);
	pid3 = fork();
	if (pid3 == -1)
		return (free_n_close(data, 8), 1);
	if (pid3 == 0)
	{
		child3(data, envp);
		perror("Error: ");
		return (free_n_close(data, 8), 1);
	}
	close(data.fdout);
	close(data.pipefd2[0]);
	free_n_close(data, 8);
	return (0);
}

int	main(int argc, char **argv, char **envp)
{
	t_data	data;

	if (argc <= 4)
		return (ft_printf("Error number arg"), 1);
	data.argc = argc;
	data.here_doc = 0;
	if (ft_strncmp(argv[1], "here_doc", 8) == 0)
	{
		here_doc(argv[2]);
		data.fdin = open(".here_doc", O_RDONLY);
		data.here_doc = 1;
	}
	else
	{
		data.fdin = open(argv[1], O_RDONLY);
		if (data.fdin == -1)
			return (perror("Error: "), 1);
	}
	if (pipex(data, argv, envp) != 0)
	{
		waitpid(-1, NULL, 0);
		return (perror("Error: "), 1);
	}
	if (data.here_doc == 1)
		unlink(".here_doc");
	waitpid(-1, NULL, 0);
	return (0);
}
