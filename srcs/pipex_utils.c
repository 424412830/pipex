/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/02 15:14:58 by abinet            #+#    #+#             */
/*   Updated: 2023/08/07 18:01:31 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pipex.h"

t_data	set_cmd1(t_data data, char **argv, char **envp)
{
	if (pipe(data.pipefd) == -1)
	{
		free_n_close(data, 1);
		exit (1);
	}
	data.cmd1 = ft_split(argv[2], ' ');
	if (!(data.cmd1))
	{
		free_n_close(data, 2);
		exit (1);
	}
	if (ft_strchr(data.cmd1[0], '/'))
		data.path_cmd1 = ft_strdup(data.cmd1[0]);
	else
	{
		data.path_cmd1 = find_path(data.cmd1[0], envp);
		if (!(data.path_cmd1))
			free_n_close(data, 3);
	}
	return (data);
}

t_data	set_cmd2(t_data data, char **argv, char **envp)
{
	data.fdout = open(argv[4], O_CREAT | O_RDWR | O_TRUNC, 0777);
	if (data.fdout == -1)
	{
		perror("Error: ");
		free_n_close(data, 5);
		exit (1);
	}
	data.cmd2 = ft_split(argv[3], ' ');
	if (!(data.cmd2))
	{
		free_n_close(data, 6);
		exit (1);
	}
	if (ft_strchr(data.cmd2[0], '/'))
		data.path_cmd2 = ft_strdup(data.cmd2[0]);
	else
	{
		data.path_cmd2 = find_path(data.cmd2[0], envp);
		if (!(data.path_cmd2))
		{
			free_n_close(data, 7);
			exit (1);
		}
	}
	return (data);
}

char	**find_all_paths(char **envp)
{
	unsigned int	i;
	char			**all_paths;

	all_paths = NULL;
	i = 0;
	while (envp[i])
	{
		if (envp[i][0] == 'P' && envp[i][1] == 'A'
		&& envp[i][2] == 'T' && envp[i][3] == 'H')
			all_paths = ft_split(envp[i], ':');
		i++;
	}
	return (all_paths);
}

char	*find_path(char *infile, char **envp)
{
	unsigned int	i;
	char			**all_paths;
	char			*path;
	char			*end_path;

	all_paths = find_all_paths(envp);
	if (!all_paths)
		return (0);
	i = 0;
	end_path = ft_strjoin("/", infile);
	while (all_paths[i])
	{
		path = ft_strjoin(all_paths[i], end_path);
		if (access(path, F_OK) == 0)
			return (free(end_path), free_tab(all_paths), path);
		free(path);
		i++;
	}
	free(end_path);
	free_tab(all_paths);
	return (0);
}
