/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/07/25 09:23:46 by abinet            #+#    #+#             */
/*   Updated: 2023/08/07 17:17:59 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pipex.h"

void	child(t_data data, char **envp)
{
	close(data.pipefd[0]);
	dup2(data.fdin, STDIN_FILENO);
	close(data.fdin);
	dup2(data.pipefd[1], STDOUT_FILENO);
	close(data.pipefd[1]);
	execve(data.path_cmd1, data.cmd1, envp);
}

void	child2(t_data data, char **envp)
{
	close(data.pipefd[1]);
	dup2(data.pipefd[0], STDIN_FILENO);
	close(data.pipefd[0]);
	dup2(data.fdout, STDOUT_FILENO);
	close(data.fdout);
	execve(data.path_cmd2, data.cmd2, envp);
}

int	pipex(t_data data, char **argv, char **envp)
{
	pid_t	pid;

	data = set_cmd1(data, argv, envp);
	pid = fork();
	if (pid == -1)
		return (free_n_close(data, 4), 1);
	if (pid == 0 && data.path_cmd1)
	{
		child(data, envp);
		return (free_n_close(data, 4), 1);
	}
	else if (pid == 0)
		return (free_n_close(data, 5), 1);
	if (data.fdin != -1)
		close(data.fdin);
	close(data.pipefd[1]);
	pipex2(data, argv, envp);
	return (0);
}

int	pipex2(t_data data, char **argv, char **envp)
{
	pid_t	pid2;

	data = set_cmd2(data, argv, envp);
	pid2 = fork();
	if (pid2 == -1)
		return (free_n_close(data, 8), 1);
	if (pid2 == 0)
	{
		child2(data, envp);
		return (free_n_close(data, 8), 1);
	}
	free_n_close(data, 9);
	return (0);
}

int	main(int argc, char **argv, char **envp)
{
	t_data	data;

	if (argc != 5)
		return (ft_printf("Error number arg"), 1);
	data.fdin = open(argv[1], O_RDONLY);
	if (data.fdin == -1)
	{
		perror("Error: ");
		if (pipex2(data, argv, envp) != 0)
			return (1);
	}
	else if (pipex(data, argv, envp) != 0)
		return (1);
	waitpid(-1, NULL, 0);
	return (0);
}
