/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clean_bonus.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abinet <abinet@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/08/05 22:06:00 by abinet            #+#    #+#             */
/*   Updated: 2023/08/07 15:50:36 by abinet           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pipex_bonus.h"

void	free_tab(char **res)
{
	int	i;

	i = 0;
	while (res[i])
	{
		free(res[i]);
		i++;
	}
	free(res);
}

void	free_n_close(t_data data, int val_error)
{
	if (val_error >= 1 && val_error < 5)
		close(data.fdin);
	if (val_error >= 2 && val_error < 5)
		close(data.pipefd[1]);
	if (val_error >= 2)
		close(data.pipefd[0]);
	if (val_error >= 3)
		free_tab(data.cmd1);
	if (val_error >= 4)
		free(data.path_cmd1);
	if (val_error >= 6)
		close(data.fdout);
	if (val_error >= 7)
		free_tab(data.cmd3);
	if (val_error >= 8)
		free(data.path_cmd3);
	if (val_error >= 8)
		free(data.path_cmd2);
}
